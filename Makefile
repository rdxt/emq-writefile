PROJECT = emq_writefile
PROJECT_DESCRIPTION = EMQ Writefile
PROJECT_VERSION = 2.3.0

DEPS = lager octopus clique

dep_lager = git https://github.com/basho/lager master
dep_octopus = git https://github.com/erlangbureau/octopus master
dep_clique  = git https://github.com/emqtt/clique

BUILD_DEPS = emqttd cuttlefish
dep_emqttd     = git https://github.com/emqtt/emqttd master
dep_cuttlefish = git https://github.com/emqtt/cuttlefish

NO_AUTOPATCH = cuttlefish

ERLC_OPTS += +debug_info
ERLC_OPTS += +'{parse_transform, lager_transform}'

COVER = true

include erlang.mk

app:: rebar.config

app.config::
	./deps/cuttlefish/cuttlefish -l info -e etc/ -c etc/emq_writefile.conf -i priv/emq_writefile.schema -d data