## Overview

Write mqtt messages straight to file.

## Configure Plugin

File: etc/plugin.config

```erlang
[
  {emq_writefile, [
    %% "#" or <<"topic1/#">> or [<<"flow/#">>, <<"car/#">>]
    {topics, "#"},
    %% sender pool size
    {pool_size, 5}
  ]}
].
```


## Load Plugin

```
./bin/emqttd_ctl plugins load emq_writefile
```