-module(emq_writefile).
-export([load/1, unload/0]).

-include_lib("emqttd/include/emqttd.hrl").

-include_lib("emqttd/include/emqttd_protocol.hrl").

-include_lib("emqttd/include/emqttd_internal.hrl").

%% Hooks functions
-export([on_message_publish/2]).

%% Called when the plugin application start
load(Env) ->
    emqttd:hook('message.publish', fun ?MODULE:on_message_publish/2, [Env]).

%% transform message and return
on_message_publish(Message = #mqtt_message{topic = <<"$SYS/", _/binary>>}, _Env) ->
    {ok, Message};
on_message_publish(Message, _Env) ->
    Topic = Message#mqtt_message.topic,
    Payload = Message#mqtt_message.payload,
    Fun = fun(Worker) ->
        ok = emq_writefile_worker:add_queue(Worker, Topic, Payload)
    end,
    ok = octopus:perform(writefile, Fun),
    {ok, Message}.

%% Called when the plugin application stop
unload() ->
    emqttd:unhook('message.publish', fun ?MODULE:on_message_publish/2).


