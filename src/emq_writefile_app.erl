-module(emq_writefile_app).

-behaviour(application).

%% Application callbacks
-export([start/2, stop/1]).

%% ===================================================================
%% Application callbacks
%% ===================================================================

start(_StartType, _StartArgs) ->
  {ok, Sup} = emq_writefile_sup:start_link(),
  ok = start_pool(),
  _ = emq_writefile:load(application:get_all_env()),
  emq_writefile_config:register(),
  {ok, Sup}.

stop(_State) ->
  emq_writefile:unload(),
  emq_writefile_config:unregister().

start_pool() ->
  PoolSize = application:get_env(emq_writefile, pool_size, 1),
  PoolOpts = [
    {pool_size, PoolSize},
    {worker, emq_writefile_worker}
  ],
  ok = octopus:start_pool(writefile, PoolOpts, []).


